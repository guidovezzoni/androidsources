This project collects pieces of code (currently only one) coming from my sources.

# SectionsFragment #
[SectionsFragment](https://bitbucket.org/guidovezzoni/androidsources/src/081bdae0c0ffcc76f56c2998c3bc8ef5025a534a/SectionsFragment/?at=master) contains a fragment from my app [Know Your Onions](https://play.google.com/store/apps/details?id=com.guidovezzoni.knowyouronions)



### Contact info

Should you need any info, please feel free to contact me [here](guido.vez@gmail.com).
