package com.guidovezzoni.knowyouronions;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ListFragment;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CursorAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import com.guidovezzoni.knowyouronions.libmrv.ActionModeHelper;
import com.guidovezzoni.knowyouronions.libmrv.IMrvFragment;
import com.guidovezzoni.knowyouronions.libmrv.ListViewSelectionHelper;

/**
 * A fragment representing a list of Items.
 * <p>
 * <p>
 * Activities containing this fragment MUST implement the {@link ISectionsFragmentListener}
 * interface.
 */

// TODO I should provide a way to change sections' order


public class SectionsFragment extends ListFragment implements ActionModeHelper.IActionModeCaller,
        AdapterView.OnItemLongClickListener, IMrvFragment {
    // Tag
    private static final String TAG_ROOT = "SectionsFragment";

    public static final String tag(long aBookID, long parentSection) {
        return TAG_ROOT + "_BookID=" + Long.toString(aBookID) + "_Parent=" + Long.toString(parentSection);
    }

    // The fragment arguments
    protected static final String ARG_NAV_DRAWER_POSITION = "nav_drawer_position";
    protected static final String ARG_ACTION_BAR_TITLE = "action_bar_title";
    protected static final String ARG_BOOK_CODE = "book_code";
    protected static final String ARG_PARENT_SECTION = "parent_section";

    // these values are instance specific
    private long mBookCode = -1;
    private long mParentSection = -1;


    // The fragment status to save
    private static final String SAVED_INSTANCE_STATE_LIST_MODE = "saved_instance_state_list_mode";

    private ISectionsFragmentListener mListener;


// *********************************************************************************************
// Lifecycle overrides - fragments/activities

    public static SectionsFragment newInstance(int navDrawerPosition, String actionBarTitle, long bookCode, long parentSection) {
        SectionsFragment fragment = new SectionsFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_NAV_DRAWER_POSITION, navDrawerPosition);
        args.putString(ARG_ACTION_BAR_TITLE, actionBarTitle);
        args.putLong(ARG_BOOK_CODE, bookCode);
        args.putLong(ARG_PARENT_SECTION, parentSection);
        fragment.setArguments(args);
        return fragment;
    }

    // Mandatory empty constructor for the fragment manager
    public SectionsFragment() {
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (ISectionsFragmentListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement " + ISectionsFragmentListener.class.getName());
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mBookCode = getArguments().getLong(ARG_BOOK_CODE);
            mParentSection = getArguments().getLong(ARG_PARENT_SECTION);
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // obtain the database  helper from the application context
        // TODO should this go in a static member to avoid memory leaks?
        DatabaseHelper mHelper = DatabaseHelper.getInstance(getActivity());
        Cursor mCursor = mHelper.getSectionsCursorsManager().getCursor(mBookCode, mParentSection);

        // set adapter and load cursor if it isn't already loaded
        SimpleCursorAdapter adapter =
                new SimpleCursorAdapter(getActivity(),
                        R.layout.list_item_sections,
                        mCursor, new String[]{
                        SectionsTableHelper.TableContract.COLUMN_NAME_NAME},
                        new int[]{R.id.title},
                        0);
        setListAdapter(adapter);

        // to avoid reloading on configuration change
        if (mCursor == null)
            new LoadCursorTask().execute();

        ListView listView = getListView();
        listView.setOnItemLongClickListener(this);
        listView.setMultiChoiceModeListener(new ActionModeHelper(this, getActivity(), R.menu.fragment_sections_cab));

        // handles config change
        int choiceMode =
                (savedInstanceState == null ? ListView.CHOICE_MODE_NONE
                        : savedInstanceState.getInt(SAVED_INSTANCE_STATE_LIST_MODE));
        listView.setChoiceMode(choiceMode);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // Indicate that this fragment would like to influence the set of actions in the action bar.
        setHasOptionsMenu(true);
    }

    @Override
    public void onSaveInstanceState(Bundle state) {
        super.onSaveInstanceState(state);

        // getListView() will crash if the view itself doesn't exist
        if (getView() != null && getListView() != null)
            state.putInt(SAVED_INSTANCE_STATE_LIST_MODE, getListView().getChoiceMode());
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

// *********************************************************************************************
// Other overrides

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        l.setItemChecked(position, true);

        if (null != mListener) {
            // Notify the active callbacks interface (the activity, if the
            // fragment is attached to one) that an item has been selected.
            TextView name = (TextView) v.findViewById(R.id.title);
            mListener.onSectionClicked(mBookCode, name.getText().toString(), id);
        }
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        // long click enters in choice mode
        getListView().setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        getListView().setItemChecked(position, true);
        return (true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // If the drawer is open, show the nav_drawer app actions in the action bar - as per google guidelines
        if (!((MainActivity) getActivity()).isNavDrawerOpen()) {
            inflater.inflate(R.menu.fragment_sections, menu);

            // disable the add content btn if:
            // - there are already sections
            // - higher in the hierarchy there is only the book name: we need to have at least 1 section level
            boolean menuEnabled = (getListView().getCount() < 1) &&
                    (mParentSection > 0);
            MenuItem menuItem = menu.findItem(R.id.action_section_content_add).setEnabled(menuEnabled);
            // gray it out
            menuItem.getIcon().setAlpha(menuEnabled ? 255 : 64);
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_section_add:
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                // Set an EditText view to get user input
                final EditText input = new EditText(getActivity());

                builder.setTitle(R.string.dialog_add_section_title).
                        setMessage(R.string.dialog_add_section_message).
                        setView(input).
                        setPositiveButton(R.string.dialog_ok_button, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                new InsertSectionTask().execute(new SectionModel(mBookCode,
                                        input.getText().toString(),
                                        mParentSection));
                            }
                        })
                        .setNegativeButton(R.string.dialog_cancel_button, null).show();
                return true;
            case R.id.action_content_add_text:
//                Toast.makeText(getActivity(), "content", Toast.LENGTH_LONG).show();
                mListener.onAddContentClicked(mBookCode, getActionBarTitle(), mParentSection, ContentTextModel.ContentType.TEXT);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    // *********************************************************************************************
// Interfaces implementation - Action Mode Helper Implementation

    @Override
    public void onCreateActionModeHelper() {

    }

    @Override
    public int getSelectedCountHelper() {
        return getListView().getCheckedItemCount();
    }

    @Override
    public boolean onActionItemClickedHelper(ActionMode mode, MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_section_delete:
                // this "if" statement assumes that EntryName is the same as key for confirmation_section_delete - see xml
                // which is not great, but better than using a hardcoded string
                if (PreferenceManager.getDefaultSharedPreferences(getActivity()).
                        getBoolean(getResources().getResourceEntryName(R.id.confirmation_section_delete), true)) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage(R.string.dialog_confirm_delete_section_message)
                            .setPositiveButton(R.string.dialog_yes_button, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    new DeleteSectionTask().execute(getListView().getCheckedItemPositions());
                                }
                            })
                            .setNegativeButton(R.string.dialog_no_button, null).show();
                    // update CAB performed after the thread execution
                } else {
                    new DeleteSectionTask().execute(getListView().getCheckedItemPositions());
                }
                return true;
            case R.id.action_section_select_all:
                ListViewSelectionHelper.selectAll(getListView());
                return true;
            case R.id.action_section_clear_selection:
                ListViewSelectionHelper.clearSelection(getListView());
                return true;
            case R.id.action_section_invert_selection:
                ListViewSelectionHelper.invertSelection(getListView());
                return true;
            default:
                return false;
        }
    }

    @Override
    public void onDestroyActionModeHelper() {
        getListView().setChoiceMode(ListView.CHOICE_MODE_NONE);
        getListView().setAdapter(getListView().getAdapter());
    }


// *********************************************************************************************
// Interfaces implementation

    @Override
    public boolean isActionModeActive() {
//        is this redundant?
        return getListView().getCheckedItemCount() > 0 || getListView().getChoiceMode() != ListView.CHOICE_MODE_NONE;
    }

    @Override
    public boolean finishActionMode() {
        boolean status = isActionModeActive();
        getListView().setChoiceMode(ListView.CHOICE_MODE_NONE);
        return status;
    }

    @Override
    public int getNavDrawerPosition() {
        return getArguments().getInt(ARG_NAV_DRAWER_POSITION);
    }

    @Override
    public String getActionBarTitle() {
        return getArguments().getString(ARG_ACTION_BAR_TITLE);
    }

    @Override
    public boolean popExtraFragment() {
        return false;
    }

// *********************************************************************************************
// Callback Interface Definitions

    public interface ISectionsFragmentListener {
        public void onSectionClicked(long aBookID, String sectionName, long parentSection);

        public void onAddContentClicked(long aBookID, String contentTitle, long parentSection, ContentTextModel.ContentType contentType);
    }

    // *********************************************************************************************
    // Background Tasks

    abstract private class BaseTask<T> extends AsyncTask<T, Void, Cursor> {
        protected DatabaseHelper mHelper;

        @Override
        protected void onPreExecute() {
            mHelper = DatabaseHelper.getInstance(getActivity());
        }

        @Override
        public void onPostExecute(Cursor result) {
            ((CursorAdapter) getListAdapter()).changeCursor(result);
            getActivity().invalidateOptionsMenu();
        }

        protected Cursor reloadDataset() {
            return (mHelper.getSectionsCursorsManager().getNewCursor(mBookCode, mParentSection));
        }
    }

    private class LoadCursorTask extends BaseTask<Void> {
        @Override
        protected Cursor doInBackground(Void... params) {
            return (reloadDataset());
        }
    }

    private class DeleteSectionTask extends BaseTask<SparseBooleanArray> {

        @Override
        protected Cursor doInBackground(SparseBooleanArray... params) {
            ListView listView = getListView();

            // iterate from the bottom of the list to prevent the index to change due to delition
            for (int ii = params[0].size() - 1; ii >= 0; ii--) {
                if (params[0].valueAt(ii)) {
                    mHelper.getSectionTableHelper().sqlDeleteSectionByID(mHelper.getWritableDatabase(),
                            listView.getItemIdAtPosition(params[0].keyAt(ii))
                    );
                }
            }
            return (reloadDataset());
        }

        @Override
        public void onPostExecute(Cursor result) {
            finishActionMode();
            super.onPostExecute(result);
        }
    }

    private class InsertSectionTask extends BaseTask<SectionModel> {
        @Override
        protected Cursor doInBackground(SectionModel... params) {
            mHelper.getSectionTableHelper().sqlCreateSection(
                    mHelper.getWritableDatabase(), params[0]);
            return reloadDataset();
        }
    }
}
