# Quick description of SectionsFragment.java

This class belongs to the app available on Google Play at [Know Your Onions](https://play.google.com/store/apps/details?id=com.guidovezzoni.knowyouronions)

"SectionsFragment - Screenshot.png" is a screenshot from this fragment running into the app.

SectionsFragment.class collects all children Sections of a given Book or ParentSection - see Books&Sections.png

Each instance is characterised by BookID and ParentSectionID, which are also stored in a TAG for the FragmentManager.

Cursors are managed separately so fragments can be recycled or destroyed without having to reload from DB every time.

Please note that any reference in the code to the word "Book", refers to what in the App UI is defined as "Onion" - I wasn't sure about the whole Onion metaphor until the publication, so I collected all the onion occurrences in strings.xm


### Contact info

Should you need any info, please feel free to contact me [here](guido.vez@gmail.com).

